#!/usr/bin/env bash

set -e

ensure_mod() {
    FILE="${1}"
    MOD="${2}"
    U_ID="${3}"
    G_ID="${4}"

    chmod "${MOD}" "${FILE}"
    chown "${U_ID}"."${G_ID}" "${FILE}"
}

if [[ ! "$(ls -A /etc/ssh)" ]]; then
    cp -a "${CACHED_SSH_DIRECTORY}"/* /etc/ssh/.
fi
rm -rf "${CACHED_SSH_DIRECTORY}"

ssh-keygen -A 1>/dev/null

echo "root:${ROOT_PASSWORD}" | chpasswd &>/dev/null

sed -i "s/#PermitRootLogin.*/PermitRootLogin\ yes/" /etc/ssh/sshd_config

printf "\n"

exec /usr/sbin/sshd -D -e "$@"