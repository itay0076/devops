from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
import sys

if len(sys.argv) > 1:
    url= sys.argv[1]
else:
    url="http://hercules:80/"

print(url)
req = Request(url)
try:
    response = urlopen(req)
except HTTPError as e:
    print('Web page is down, Error code: ', e.code)
except URLError as e:
    print('Web page is down, Reason: ', e.reason)
else:
    print("Web page is up")